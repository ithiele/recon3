% create SBML file for Recon 3


writeCbModel(modelRecon3,'sbml','modelRecon3',{'c','e','l','r','x','n','m','g'},{'Cytosol','Extracellular Space','Lysosome','Endoplasmatic Reticulum','Peroxisome','Nucleus','Mitochondrion','Golgi Apparatus'},2,1)

writeCbModel(modelRecon3model,'sbml','modelRecon3model',{'c','e','l','r','x','n','m','g'},{'Cytosol','Extracellular Space','Lysosome','Endoplasmatic Reticulum','Peroxisome','Nucleus','Mitochondrion','Golgi Apparatus'},2,1)
