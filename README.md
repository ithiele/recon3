Recon3
======

Branches:

1. `master` -- [click here](https://git-r3lab.uni.lu/MSP/Recon3/tree/Recon3_generation)
2. `Recon3_generation` (protected): [click here](https://git-r3lab.uni.lu/MSP/Recon3/tree/Recon3_generation)
3. `old` (protected): [click here](https://git-r3lab.uni.lu/MSP/Recon3/tree/old)

Please note that the branches `Recon3_generation` and `old` should **never** be merged to `master` and are not further maintained.
